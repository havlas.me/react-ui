module.exports = {
  'presets': [
    ['@babel/env', {'targets': '> 0.25%, not dead'}],
    ['@babel/preset-react'],
  ],
  'comments': false,
  'minified': true,
}

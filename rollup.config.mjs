import {babel} from '@rollup/plugin-babel'
import commonjs from '@rollup/plugin-commonjs'
import {nodeResolve} from '@rollup/plugin-node-resolve'
import terser from '@rollup/plugin-terser'
import externals from 'rollup-plugin-node-externals'
import packagej from './package.json' assert {type: 'json'}

export default [
  {
    input: 'lib/headless/index.js',
    output: [
      {
        file: packagej.main,
        format: 'cjs',
        exports: 'named',
        sourcemap: true,
      },
      {
        file: packagej.module,
        format: 'esm',
        exports: 'named',
        sourcemap: true,
      },
    ],
    plugins: [
      externals(),
      nodeResolve(),
      babel(),
      commonjs(),
      terser(),
    ],
    watch: {
      include: 'lib/**',
    },
  },
]

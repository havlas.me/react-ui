const View = function ({as: Component = 'div', children, uses, ...rest}) {
  return (
    <Component {...rest}>
      {children}
    </Component>
  )
}

export default View

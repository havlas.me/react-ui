import {voidable} from '@havlasme/react-toolkit'
import View from '../View'

const usesUtility = function ({outbound}) {
  return {
    'data-outbound': !!outbound,
    'target': voidable(outbound, typeof outbound !== 'string'),
    'rel': voidable('nofollow noopener noreferrer', !outbound),
  }
}

const Link = function ({children, outbound, ...rest}) {
  return (
    <View as="a" {...usesUtility({outbound})} {...rest}>
      {children}
    </View>
  )
}

export default Link

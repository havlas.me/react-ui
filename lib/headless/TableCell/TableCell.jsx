import View from '../View'

const TableCell = function ({children, ...rest}) {
  return (
    <View as="td" {...rest}>
      {children}
    </View>
  )
}

export default TableCell

import {runCallable, useBoolState} from '@havlasme/react-toolkit'
import {useCallback} from 'react'
import View from '../View'

const usesUtility = function ({touched}) {
  return {
    'data-touched': touched === true,
  }
}

const Input = function ({onFocus, ...rest}) {
  // the touched state. set to true on the first `focus` event
  const [touched, setTouched] = useBoolState(false)

  // the focus event callback.
  // set the `touched` flag to true; dispatch the `onFocus` callback with the event
  const onFocusCallback = useCallback(
    function (event, ...rest) {
      const dataset = event.currentTarget.dataset

      if (dataset.touched !== 'true') {
        setTouched(true)
      }
      return runCallable(onFocus, event, ...rest)
    }, [onFocus])

  return (
    <View as="input" onFocus={onFocusCallback} {...usesUtility({touched})} {...rest}/>
  )
}

export default Input

import View from '../View'

const FormControl = function ({children, ...rest}) {
  return (
    <View {...rest}>
      {children}
    </View>
  )
}

export default FormControl

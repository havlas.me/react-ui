import React from 'react'
import View from '../View'

const usesAccessibility = function () {
  return {
    'role': 'menu',
    'tabindex': 0,
  }
}

const usesUtility = function ({open}) {
  return {
    'data-ui-state': open === true ? 'open' : '',
  }
}

const Menu = function ({children, open, onClick, ...rest}) {
  return open === true ? (
    <View {...usesAccessibility()} {...usesUtility({open})} {...rest}>
      {React.Children.map(children, function (child) {
        return !React.isValidElement(child) ? null
          : React.cloneElement(child, {onClick: onClick})
      })}
    </View>
  ) : null
}

export default Menu

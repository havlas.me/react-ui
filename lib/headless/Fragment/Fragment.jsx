import {Fragment as Component} from 'react'

const Fragment = function ({children}) {
  return (
    <Component>
      {children}
    </Component>
  )
}

export default Fragment

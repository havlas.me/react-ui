import {runCallable, useBoolState} from '@havlasme/react-toolkit'
import {useCallback} from 'react'
import View from '../View'

const usesUtility = function ({submitted}) {
  return {
    'data-submitted': submitted === true,
  }
}

const Form = function ({children, onSubmit, ...rest}) {
  // the submission state. set true on first `submit` event
  const [submitted, setSubmitted] = useBoolState(false)

  // the submit event callback
  // set the `submitted` flag to true; dispatch the `onSubmit` callback with event
  const onSubmitCallback = useCallback(
    function (event, ...rest) {
      const dataset = event.currentTarget.dataset

      if (dataset.submitted !== 'true') {
        setSubmitted(true)
      }
      return runCallable(onSubmit, event, ...rest)
    }, [onSubmit, setSubmitted])

  return (
    <View as="form" onSubmit={onSubmitCallback} {...usesUtility({submitted})} {...rest}>
      {children}
    </View>
  )
}

export default Form

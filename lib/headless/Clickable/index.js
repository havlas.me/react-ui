import {runCallable} from '@havlasme/react-toolkit'
import {is, isNil} from 'ramda'
import React, {useCallback} from 'react'
import lastEventCallback from '../lastEventCallback'
import View from '../View'
import voidable from '../voidable'

const usesAccessibility = function ({disabled}) {
  return {
    'role': 'button',
    'aria-disabled': disabled === true,
  }
}

const usesUtility = function ({as, disabled, value}) {
  return {
    'type': voidable('button', !isNil(as)),
    'data-disabled': disabled === true,
    'data-serialized': !isNil(value) && !is(String, value),
    'data-value': value,
  }
}

const Clickable = function ({children, onClick, ...rest}) {
  // the click event callback
  // do not run the callback when disabled (`disabled` prop set to true)
  // resolve the dataset and send to the callback as second argument
  const onClickCallback = useCallback(function (event, ...rest) {
    lastEventCallback(event)

    if (event.currentTarget.dataset.disabled !== 'true') {
      return runCallable(onClick, event, event.currentTarget.dataset, ...rest)
    }
  }, [onClick])

  return (
    <View as="button" onClick={onClickCallback} {...usesAccessibility(rest)} {...usesUtility(rest)} {...rest}>
      {children}
    </View>
  )
}

export default Clickable

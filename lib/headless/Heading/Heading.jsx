import View from '../View'

const Heading = function ({children, ...rest}) {
  return (
    <View as="h2" {...rest}>
      {children}
    </View>
  )
}

export default Heading

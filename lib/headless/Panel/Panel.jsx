import View from '../View'

const Panel = function ({children, className, containerClassName, ...rest}) {
  return (
    <View as="section" className={containerClassName} {...rest}>
      <View className={className}>
        {children}
      </View>
    </View>
  )
}

export default Panel

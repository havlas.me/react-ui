import Clickable from '../Clickable'

const usesAccessibility = function () {
  return {
    'role': 'menuitem',
    'tabindex': -1,
  }
}

const MenuItem = function ({children, ...rest}) {
  return (
    <Clickable {...usesAccessibility()} {...rest}>
      {children}
    </Clickable>
  )
}

export default MenuItem

import View from '../View'

const InputLabel = function ({children, ...rest}) {
  return (
    <View as="label" {...rest}>
      {children}
    </View>
  )
}

export default InputLabel

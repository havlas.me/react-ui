import View from '../View'

const Table = function ({children, ...rest}) {
  return (
    <View as="table" {...rest}>
      {children}
    </View>
  )
}

export default Table

import View from '../View'

const TableRow = function ({children, ...rest}) {
  return (
    <View as="tr" {...rest}>
      {children}
    </View>
  )
}

export default TableRow

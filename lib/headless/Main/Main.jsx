import View from '../View'

const Main = function ({children, ...rest}) {
  return (
    <View as="main" {...rest}>
      {children}
    </View>
  )
}

export default Main

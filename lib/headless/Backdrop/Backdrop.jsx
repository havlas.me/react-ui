import View from '../View'

const Backdrop = function ({ children, ...rest }) {
  return (
    <View {...rest}>
      {children}
    </View>
  )
}

export default Backdrop

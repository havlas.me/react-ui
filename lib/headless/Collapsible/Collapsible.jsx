import {useBoolState} from '@havlasme/react-toolkit'
import View from '../View'

const usesUtility = function ({open}) {
  return {
    'data-open': open === true,
  }
}

const Collapsible = function ({children, defaultOpen = false, ...rest}) {
  // collapsible state.
  const [open, setOpen] = useBoolState(defaultOpen)

  return (
    <View {...usesUtility({open})} {...rest}>
      {children(open, setOpen)}
    </View>
  )
}

export default Collapsible

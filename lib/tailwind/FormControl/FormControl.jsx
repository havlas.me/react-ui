import cc from 'classcat'
import {FormControl as Component} from '../../headless'

const FormControl = function ({children, className, ...rest}) {
  return (
    <Component className={cc(['transition', className])} {...rest}>
      {children}
    </Component>
  )
}

export default FormControl

import cc from 'classcat'
import {Menu as Component} from '../../headless'

const Menu = function ({children, className, ...rest}) {
  return (
    <Component className={cc(['group/ui-menu', className])} {...rest}>
      {children}
    </Component>
  )
}

export default Menu

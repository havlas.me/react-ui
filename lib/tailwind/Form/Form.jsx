import cc from 'classcat'
import {Form as Component} from '../../headless'

const Form = function ({children, className, ...rest}) {
  return (
    <Component className={cc(['group/ui-form', className])} {...rest}>
      {children}
    </Component>
  )
}

export default Form

import cc from 'classcat'
import {Clickable as Component} from '../../headless'

const Clickable = function ({children, className, ...rest}) {
  return (
    <Component className={cc(['transition cursor-pointer data-disabled:cursor-not-allowed group/ui-clickable', className])} {...rest}>
      {children}
    </Component>
  )
}

export default Clickable

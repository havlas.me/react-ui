import cc from 'classcat'
import {Collapsible as Component} from '../../headless'

const Collapsible = function ({children, className, ...rest}) {
  return (
    <Component className={cc(['group/ui-collapsible', className])} {...rest}>
      {children}
    </Component>
  )
}

export default Collapsible

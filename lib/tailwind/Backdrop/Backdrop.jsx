import cc from 'classcat'
import {Backdrop as Component} from '../../headless'

const Backdrop = function ({children, className, ...rest}) {
  return (
    <Component className={cc(['fixed inset-0 z-50 cursor-default peer/ui-backdrop', className])} {...rest}>
      {children}
    </Component>
  )
}

export default Backdrop
